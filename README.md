---
---
<meta charset="utf8"/>

# holoPass



use local agent to validate identity

i.e. present a valid "IDcard" + merchant challenge + CVV (i.e. last digits!)

there are 4 versions of ID card:
 . original secret (never goes on the network)
 . original (w/ wet-signature)
 . original (w/ coersed-signature)
 . copy (w/ dry signature)
 . original xored w/ valid mask
 . copy xored w/ copy mask
 
 
| ---------- | -------- |
| secret     | oiginal  |
| ---------- | -------- |
| ![ID][IDS] | ![IDO]   |
| ![IX][IDX] | ![IDC]   |
| ---------- | ------   |

<style>
table img { max-width: 280px; }
</style>

[IDS]: IDcards/holoIDcard-ORIG.png
[IDO]: IDcards/holoIDcard-ORIG.webp
[IDX]: IDcards/holoIDcard-XOR.png
[IDC]: IDcards/holoIDcard-COPY.jpg

