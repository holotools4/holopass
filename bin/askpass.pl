#!/usr/bin/perl

BEGIN { our $dbug = 0; eval "use YAML::Syck qw();" if ($dbug); }


# This script take identity for look for a key file
#  1. loads the key
#  2. creates a onetime pad in ~/secure/etc/otp.sec
#       otpe = encoded otp from a 128bit seed
#       otp = otpe xor key
#  3. creates a /tmp/cypher.txt file
#       cipher = pass xor otp
#
#  4. later can recover pass w/ 
#       pass = cipher xor otp

# usage askpass <id>
# assumed there is a key file: ~/secure/:identity.key
#
my $user = $ENV{USER} || 'secbot';
my $gradual = $ENV{GRADUAL_PATH} || sprintf '/home/%s/.gradual',$user;
my $vault = $ENV{VAULTDIR} || sprintf '%s/cypher',$gradual;
my $secdir = $ENV{SECDIR} || sprintf '/home/%s/secure',$user;

#--------------------------------
# -- Options parsing ...
#
my $clear = 0;
my $msg = '%s password ?';
while (@ARGV && $ARGV[0] =~ m/^-/)
{
  $_ = shift;
  #/^-(l|r|i|s)(\d+)/ && (eval "\$$1 = \$2", next);
  if (/^--?(?:va(?:ult)?|c[yi](?:pher)?)(\w*)/) { $vault = $1 ? $1 : shift; }
  elsif (/^--?cl(?:ear)?/) { $clear= 1; }
  elsif (/^-m(\w*)/) { $msg = $1 ? $1 : shift; }
  elsif (/^--?ve(?:rbose)?/) { $verbose= 1; }
  else                  { die "Unrecognized switch: $_\n"; }

}
#understand variable=value on the command line...
eval "\$$1='$2'"while $ARGV[0] =~ /^(\w+)=(.*)/ && shift;
;# --------------------------------------------------------------------------
my $identity = shift;
if ($msg =~ m/%/) {
  $msg = sprintf $msg,@ARGV;
}
printf "--- # ask pass on %s\n",&hdate($^T);
printf "id: %s\n",$identity if $verbose;

my $cryconfig = sprintf '%s/%s.cryfs',$secdir,$identity;
printf "cfg: %s\n",$cryconfig if $verbose;
# ------------------------------------
my $cipher = '1234';
our $pass = undef;
# ------------------------------------
my $cipherf = '/tmp/cipher.txt';
my $otpf = $secdir.'/etc/otp.sec';
# case 1:
#   no $secdir/etc -> ask for a mounting pass then mount partition 
#   then use or create OTP
#   and encrypt pass in /tmp/cypher
# case 2:
#   -d $secdir/etc
#      if -e /tmp/cypher use it else ask for cypher code
#      if -e OTP use it else ask for ORP code
#      then decrypt pass
#
if (! -d $secdir.'/etc') {
   print STDERR "! -d $secdir/etc\n";
   $pass = &get_pass();
   # mounting $secdir ...
   $ENV{CRYFS_FRONTEND}='noninteractive';
   open EXEC,"|cryfs -c $cryconfig $vault $secdir -- -o nonempty > /dev/null";
   print EXEC $pass ; close EXEC;

   my $key = &get_key($identity);
   printf "key: %s (%u-bit)\n",unpack('H*',$key),length($key)*8 if $clear;
   my $otp;
   if (-e $otpf) { # /!\ reuse OTP
      open F,'<',$otpf;
      local $/ = undef; my $buf = <F>; chomp($buf); close F;
      $otp = &xor(pack('H*',$buf),$key);
      close F;
   } else { # create OTP
      my $otpe = &get_seed();
      open F,'>',$otpf;
      chmod 0600, *F;
      print F unpack'H*',$otpe;
      close F;
      $otp = &xor($otpe,$key);
      printf STDERR "otpe: %s\n",unpack'H*',$otpe;
      printf STDERR "otp: %s\n",unpack'H*',$otp if $dbug;
   }
   open F,'>',$cipherf;
   my $cipher = &xor($pass,$otp);
   print F unpack'H*',$cipher;
   printf "cipher: %s\n",unpack'H*',$cipher;
   close F;

} else {
   if (-e $cipherf) {
   local *F; open F,'<',$cipherf;
   local $/ = undef; my $buf = <F>; chomp($buf); close F;
   $cipher = pack'H*',$buf;
   printf "cipher: %s\n",unpack'H*',$cipher;
   } else {
     print STDERR "! -e $cipherf\n";
     $cipher = &get_otp();
   }
   my $key = &get_key($identity);
   printf "key: %s (%u-bit)\n",unpack('H*',$key),length($key)*8 if $clear;
   if (-e $otpf) {
      open F,'<',$otpf;
      local $/ = undef; my $buf = <F>; chomp($buf); close F;
      $otp = &xor(pack('H*',$buf),$key);
      close F;
      printf STDERR "otpe: %s\n",$buf;
      printf STDERR "otp: %s\n",unpack'H*',$otp if $dbug;
   } else {
     print STDERR "! -e $otpf\n";
     $otp = &xor(&get_otp(),$key);
   }
   $pass = &xor($cipher,$otp);
   #printf "msg: '%s'\n",$msg;
}
# ------------------------------------

sleep 1;

## TBD ... ask questionnaire to authenticate and then decrypt pass 

# ------------------------
printf "pass: %s\n",$pass if $clear;
# ------------------------
exit $? if ($0 eq __FILE__);
1;


sub get_key {
   my $id = shift;
   my $keyring = $ENV{KEYRING} || sprintf '%s/keys',$secdir;
      $keyring = $secdir if (! -e $keyring);
   my $keyfile = sprintf'%s/%s.key',$keyring,$id;
   #print "keyfile: ",$keyfile;
   die qq'"$keyfile": $!' unless -e $keyfile;
   my $xml = &loadXML($keyfile);

   use MIME::Base64 qw(decode_base64);
   my $k64 = $xml->{Key}{Data};
   my $key = &decode_base64($k64);
   return $key;
}

sub get_otp {
   local $| = 1;
   my $otp;
   #if (exists $ENV{SHLVL} ) { print 'otp: '; }
   local $/ = "\n";
   my $otp16 = <STDIN>; chomp $otp16;
   $otp = pack'H*',$otp16;
   return $otp; 
}

sub get_pass {
   local $| = 1;
   my $pass = undef;
   if (1) {
     local *EXEC; open EXEC,'ssh-askpass-fullscreen $msg|';
     $pass = <EXEC>; chomp $pass; close EXEC;
   } else {
      use Term::ReadKey qw();
      if (exists $ENV{SHLVL}) {
         print 'pass: ';
         &Term::ReadKey::ReadMode(2); # cooked + no-echo
      }
      local $/ = "\n";
      $pass = <STDIN>; chomp $pass;
      if (exists $ENV{SHLVL}) {
         &Term::ReadKey::ReadMode(0); # restore mode
            print '*' x length($pass),"\n";
      }
   }
   return $pass; 
}

sub xor {
 my @a = unpack'Q*',$_[0] . "\0"x7;
 my @b = unpack'Q*',$_[1] . "\0"x7;
 my @x = ();
 foreach my $i (0 .. $#a) {
   $x[$i] = $a[$i] ^ $b[$i];
   printf "%08X = %08X ^ %08X\n",$x[$i],$a[$i],$b[$i] if $dbug;
 }
 my $x = pack'Q*',@x;
}

sub loadXML {
   my $file = shift;
   use XML::Simple qw(XMLin);
   #use XML::LibXML::Simple qw(XMLin);
   # -------------------------------------------------------------
   my %options = (
         KeepRoot => 0,
         ForceArray => 0,
         ForceContent => 0,
         #NoAttr => 0, # in+out - handy
         #ValueAttr => [ 'value' ], # in - handy
         #ValueAttr => { element => 'attribute', '...' }, # in+out - handy
         #VarAttr => 'name', ContentKey => '-content',
         KeyAttr => { 'Key' => 'Data'},
         KeyAttr => []
     );

   my $xml = XMLin($file,%options);
   printf "%s <-- $file\n",YAML::Syck::Dump($xml) if $dbug;
   return $xml;
}

sub get_seed {
   use Math::Prime::Util qw();
   $seed = &Math::Prime::Util::entropy_bytes(16); # 128bit
   return $seed;
}

# ---------------------------------------------------------
sub hdate { # return HTTP date (RFC-1123, RFC-2822) 
  my ($time,$delta) = @_;
  my $stamp = $time+$delta;
  my $tic = int($stamp);
  #my $ms = ($stamp - $tic)*1000;
  my $DoW = [qw( Sun Mon Tue Wed Thu Fri Sat )];
  my $MoY = [qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec )];
  my ($sec,$min,$hour,$mday,$mon,$yy,$wday) = (gmtime($tic))[0..6];
  my ($yr4,$yr2) =($yy+1900,$yy%100);

  # Mon, 01 Jan 2010 00:00:00 GMT
  my $date = sprintf '%3s, %02d %3s %04u %02u:%02u:%02u GMT',
             $DoW->[$wday],$mday,$MoY->[$mon],$yr4, $hour,$min,$sec;
  return $date;
}
# ---------------------------------------------------------

1;
